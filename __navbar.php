<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= $page_name=='data_list' ? 'myactive' : '' ?>">
                <a class="nav-link" href="data_list.php">列表 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item <?= $page_name=='data_insert' ? 'myactive' : '' ?>">
                <a class="nav-link" href="data_insert.php">新增 <span class="sr-only">(current)</span></a>
            </li>
        </ul>

    </div>
</nav>
<style>
    li.nav-item.myactive > a {
        background-color: #007bff;
        color: white !important;
    }
</style>