<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        td{
            width: 40px;
            height: 40px;
        }
    </style>
</head>
<body>
<!-- http://www.w3school.com.cn/php/func_string_printf.asp

可能的格式值：

%% - 返回一个百分号 %
%b - 二进制数
%c - ASCII 值对应的字符
%d - 包含正负号的十进制数（负数、0、正数）
%e - 使用小写的科学计数法（例如 1.2e+2）
%E - 使用大写的科学计数法（例如 1.2E+2）
%u - 不包含正负号的十进制数（大于等于 0）
%f - 浮点数（本地设置）
%F - 浮点数（非本地设置）
%g - 较短的 %e 和 %f
%G - 较短的 %E 和 %f
%o - 八进制数
%s - 字符串
%x - 十六进制数（小写字母）
%X - 十六进制数（大写字母）

-->

<table>
    <?php for($i=0; $i<16; $i++):
        //$c = dechex($i);
        ?>
    <tr>

<!--        <td bgcolor="#--><?php //printf("%X%X%X%X%X%X", $i, $i, $i, $i, $i, $i) ?><!--">&nbsp;</td>-->
        <td bgcolor="#0000<?php printf("%X%X", $i, $i) ?>">&nbsp;</td>

    </tr>
    <?php endfor ?>

</table>




</body>
</html>