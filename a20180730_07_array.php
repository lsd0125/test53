<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        td{
            width: 40px;
            height: 40px;
        }
    </style>
</head>
<body>
<div>
<?php
$ar = array(
    'hello',
    3 => 'abc',
    "3" => 'def',
    'name' => 'shinder',
    "pw" => "pass",
    'hi',
);

foreach($ar as $v){
    echo "$v <br>";
}

echo '-------<br>';

foreach($ar as $k => $v){
    echo "$k => $v , {$ar[$k]} <br>";
}

?>
</div>
</body>
</html>