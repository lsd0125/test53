<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>

<?php
$ar = array(
    'AA' => [
            'AA1' => '#aa1',
            'AA2' => '#aa2',
            'AA3' => '#aa3',
            'AA4' => '#aa4',
    ],
    'BB' => [
            'BB1' => '#BB1',
            'BB2' => '#BB2',
            'BB3' => '#BB3',
            'BB4' => '#BB4',
    ],
    'CC' => [
            'CC1' => '#CC1',
            'CC2' => '#CC2',
            'CC3' => '#CC3',
            'CC4' => '#CC4',
    ],
);

?>
   <div class="container">
       <nav class="navbar navbar-expand-lg navbar-light bg-light">
           <a class="navbar-brand" href="#">Navbar</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
           </button>

           <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
                   <?php foreach($ar as $k1 => $v1): ?>
                   <li class="nav-item dropdown">
                       <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <?= $k1 ?>
                       </a>
                       <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                           <?php foreach($v1 as $k2 => $v2): ?>
                           <a class="dropdown-item" href="<?= $v2 ?>"><?= $k2 ?></a>
                           <?php endforeach ?>
                       </div>
                   </li>
                   <?php endforeach ?>
               </ul>

           </div>
       </nav>



   </div>



</body>
</html>