<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        td{
            width: 40px;
            height: 40px;
        }
    </style>
</head>
<body>
<div>
<?php
$ar = array(2,3,4);
$br = array(5,6,7);


function func1($a){
    $a[] = 100;
}

function func2(&$a){
    $a[] = 200;
}

echo '<pre>';

func1($ar);
print_r($ar);

func2($ar);
print_r($ar);




echo '</pre>';



?>
</div>
</body>
</html>