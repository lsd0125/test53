<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_edit';

if(! isset($_GET['sid'])){
    header('Location: data_list.php');
    exit;
}
$sid = intval( $_GET['sid'] );

if(isset($_POST['name'])) {

    $sql = "UPDATE `address_book` SET `name`=?,`mobile`=?,`email`=?,`address`=?,`birthday`=? WHERE sid=$sid";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('sssss',
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['address'],
        $_POST['birthday']
        );

    $stmt->execute();

    $af = $stmt->affected_rows;
}



$sql = "SELECT * FROM `address_book` WHERE `sid`=$sid";

$row = $mysqli->query($sql)->fetch_assoc();
if(empty($row)){
    header('Location: data_list.php');
    exit;
}




?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <style>
        form > .form-group > small {
            display: none;
        }
    </style>

    <?php if(isset($af)): ?>

        <div class="row">
        <?php if($af==1): ?>
            <div class="col">
                <div class="alert alert-success" role="alert">
                    修改完成
                </div>
            </div>
        <?php elseif($af==0): ?>
            <div class="col">
                <div class="alert alert-warning" role="alert">
                    資料沒有修改
                </div>
            </div>
        <?php elseif($af==-1): ?>
            <div class="col">
                <div class="alert alert-danger" role="alert">
                    唯一鍵重複
                </div>
            </div>
        <?php endif ?>
        </div>
    <?php endif ?>

    <div class="row justify-content-md-center" style="margin-top: 20px" >

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">編輯資料</div>

                    <form name="form1" method="post">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?= $row['name'] ?>">
                            <small id="nameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機號碼</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?= $row['mobile'] ?>">
                            <small id="mobileHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="email">電子郵箱</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?= $row['email'] ?>">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" value="<?= $row['birthday'] ?>">
                            <small id="birthdayHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" name="address" id="address" cols="30" rows="10" placeholder="請填寫地址"><?= $row['address'] ?></textarea>
                            <small id="addressHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <button type="submit" class="btn btn-primary">修改</button>
                    </form>
                </div>


            </div>
        </div>


    </div>




</div>
<?php include __DIR__. '/__html_foot.php'; ?>