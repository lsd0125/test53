<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_insert';

if(isset($_POST['name'])) {

    $sql = "INSERT INTO `address_book`(
        `name`, `mobile`, `email`, `address`, `birthday`
        ) VALUES (?, ?, ?, ?, ?)";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('sssss',
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['address'],
        $_POST['birthday']
        );

    $stmt->execute();

    $af = $stmt->affected_rows;

    /*
    $sql = sprintf("INSERT INTO `address_book`(
        `name`, `mobile`, `email`, `address`, `birthday`
        ) VALUES ('%s', '%s', '%s', '%s', '%s')",
        $mysqli->escape_string($_POST['name']),
        $mysqli->escape_string($_POST['mobile']),
        $mysqli->escape_string($_POST['email']),
        $mysqli->escape_string($_POST['address']),
        $mysqli->escape_string($_POST['birthday'])
        );

    $mysqli->query($sql);


    $message = '新增完成';
*/


}


?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <style>
        form > .form-group > small {
            display: none;
        }
    </style>

    <?php if(isset($af)): ?>
    <div class="row">
        <div class="col">
            <div><?= $af  ?></div>
            <div class="alert alert-warning" role="alert">

            </div>
        </div>
    </div>
    <?php endif ?>

    <div class="row justify-content-md-center" style="margin-top: 20px" >

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">新增資料</div>

                    <form name="form1" method="post">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="姓名">
                            <small id="nameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機號碼</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機號碼">
                            <small id="mobileHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="email">電子郵箱</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="電子郵箱">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" placeholder="生日">
                            <small id="birthdayHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" name="address" id="address" cols="30" rows="10" placeholder="請填寫地址"></textarea>
                            <small id="addressHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <button type="submit" class="btn btn-primary">新增</button>
                    </form>
                </div>


            </div>
        </div>


    </div>




</div>
<?php include __DIR__. '/__html_foot.php'; ?>