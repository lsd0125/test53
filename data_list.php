<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_list';
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$per_page = 5;


$t_sql = "SELECT COUNT(1) FROM `address_book`";
//$t_rs = $mysqli->query($t_sql);
//$total_num = $t_rs->fetch_row()[0];
$total_num = $mysqli
    ->query($t_sql)
    ->fetch_row()[0];

$total_pages = ceil($total_num/$per_page);
$page = max($page, 1);
$page = min($page, $total_pages);



$sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC LIMIT %s, %s ", ($page-1)*$per_page, $per_page);

// echo $sql;
$result = $mysqli->query($sql);



?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">

<?php //include '__navbar.php'; ?>
<?php include __DIR__. '/__navbar.php'; ?>


    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                <a class="page-link" href="?page=<?= $page-1 ?>">Previous</a>
            </li>
            <?php for($i=1; $i<=$total_pages; $i++){ ?>
                <li class="page-item <?= $page==$i ? 'active' : '' ?>">
                    <a class="page-link" <?= $page==$i ? '' : "href=\"?page=$i\"" ?>><?= $i ?></a>
                </li>
            <?php } ?>
            <li class="page-item <?= $page==$total_pages ? 'disabled' : '' ?>">
                <a class="page-link" href="?page=<?= $page+1 ?>">Next</a>
            </li>
        </ul>
    </nav>


    <div class="" style="margin-top: 20px">

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">mobile</th>
                <th scope="col">email</th>
                <th scope="col">address</th>
                <th scope="col">birthday</th>
                <th scope="col">編輯</th>
                <th scope="col">刪除</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?= $row['sid'] ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['mobile'] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td><?= htmlentities($row['address']) ?></td>
                    <td><?= $row['birthday'] ?></td>

                    <td><a href="data_edit.php?sid=<?= $row['sid'] ?>">
                            <i class="fas fa-edit"></i>
                        </a></td>


                    <td><a href="javascript: delete_it(<?= $row['sid'] ?>)">
                            <i class="fas fa-trash-alt"></i>
                        </a></td>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>

    </div>

</div>

    <script>
        function delete_it(sid){
            if( confirm('確定要刪除編號為 '+ sid + ' 的資料嗎?')){
                location.href = "data_delete.php?sid=" + sid;
            }
        }


    </script>


<?php include __DIR__. '/__html_foot.php'; ?>